<?php 

class modulosimple extends Module {

	function __construct()
	{
        $this->name = "modulosimple";
        $this->tab = ''; // clasificacion donde va ser mostrado en el admin
        $this->version = '0.1.0';
        parent::__construct();
        $this->displayName = $this->l('modulo simple');
        $this->description = $this->l('primer modulo, solo tiene como mostrarse en el admin y decir hola');
	}
	
	function install()
	{
       if (!parent::install()
           OR !$this->registerHook('rightColumn')
           OR !$this->registerHook('leftColumn'))
                return false;
        return true;	
	}
	
	public function hookLeftColumn($params)	
	{
		echo "Hello World!";
	}
	
    public function hookRightColumn($params)
    {
        return $this->hookLeftColumn($params); // hace lo mismo que la otra columna
    }
	
	function uninstal()
	{
		if (!parent::uninstall())
			return false;
		return true;
	}

}
?>