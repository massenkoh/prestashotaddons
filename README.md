Prestashop works ..

* **minimal** : module most minimal and simple, only the install/uninstall and get value from config, no view or forms
* **example**: module most simple, congifs, view and forms, from https://github.com/PrestaEdit/Canvas-Module-Prestashop-15
* **tallerorder**: implement a minimal basic workshop, for repair products that still not are in the shop

# Source informations:

- most simple example of module:
  - 1.6: https://github.com/PrestaEdit/PrestaShop-Canvas/tree/master/PrestaShop%201.6/Modules/simple
  - 1.5 y 1.6 https://github.com/PrestaEdit/Canvas-Module-Prestashop-15
- create menu entries in backend:
  - https://www.prestashop.com/forums/topic/249135-how-to-create-new-tab-in-admin/
  - https://github.com/PrestaEdit/Canvas-Module-Prestashop-15/blob/master/example.php#L144
  - https://www.prestashop.com/forums/topic/157626-how-does-admin-tabs-mvc-work/
- crear y entender modelos de prestasho (importante si es para formularios)
  - http://proactive-learning.readthedocs.io/es/latest/php/prestashop/objectmodels.html

# Developer documentation

Everybody know that prestasho its a shit! so lest destroy that, putting info here, also due the secret of 
prestashop are change the api, all of you must mantain the same version and never update..
As most of that info can be dissaper from the net, lest get into in more order:

* [Prestashop-Minimal-Module](docs/prestashop-1-minimal-module.md)
* [Prestashop-Object-Models](docs/prestashop-2-object-models.md)
