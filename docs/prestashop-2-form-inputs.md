# Prestashop forms inputs and List

* https://stackoverflow.com/questions/27731117/prestashop-set-multiple-select-from-the-module-and-get-them-in-the-input

Dummy question but I cannot seem to find a straightforward answer. I am building a form using the HelperForm class and I want to pass a hidden field with it for which I use:

	    array(
	    'type' => 'hidden',
	    'value' => 43,
	    'name' => 'id_product'
	    ),
	   )
	  ); 

Where value seems to invalid key in the array and doesn`t work. I saw the values array but it is said that it is used for radio buttons only.

 

How am I suppose to assign value to the input field?

 

EDIT: Found the solution myself. Inserted this when configuring the helper:

$helper->fields_value['id_product'] = Tools::getValue('id_product');


