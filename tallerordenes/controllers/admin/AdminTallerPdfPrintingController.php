<?php
/*
*  @author PICCORO
*  @copyright  2017
*/

class AdminTallerPdfPrintingController extends FrontController
{
    public $php_self = 'admin-taller-pdf-order-return';
    protected $display_header = false;
    protected $display_footer = false;

    public function postProcess()
    {
        $from_admin = (Tools::getValue('adtoken') == Tools::getAdminToken('AdminReturn'.(int)Tab::getIdFromClassName('AdminReturn').(int)Tools::getValue('id_employee')));

        if (!$from_admin && !$this->context->customer->isLogged()) {
            Tools::redirect('index.php?controller=authentication&back=tallerorden-follow');
        }

        if (Tools::getValue('id_tallerorden_return') && Validate::isUnsignedId(Tools::getValue('id_tallerorden_return'))) {
            $this->tallerorden = new TallerOrden(Tools::getValue('id_tallerorden_return'));
        }

        if (!isset($this->tallerorden) || !Validate::isLoadedObject($this->tallerorden)) {
            die(Tools::displayError('Order return not found.'));
        }/* elseif (!$from_admin && $this->tallerorden->id_employee != $this->context->id_employee) {
            die(Tools::displayError('LA orden de taller no esta asignada a ud.'));
        }*/
    }

    public function display()
    {
        $pdf = new PDF($this->tallerorden, PDF::TEMPLATE_TALLERORDEN_RETURN, $this->context->smarty);
        $pdf->render();
    }
}
