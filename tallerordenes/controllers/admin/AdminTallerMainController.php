<?php

/**
 * Tab Taller - Controller Admin para taller de servicios de reparaciones (producto entra y sale , no venta)
 *
 * @author     	Sergio Párraga <computersystemsunic@gmail.com>, PICCORO Lenz McKAY <mckaygerhard@gmail.com>
 * @copyright  	2017 PICCORO Lenz McKAY <mckaygerhard@gmail.com>
 * @version   	1.0.1
 * @link       	http://datasystems.com.ve
 * @since      	File available since Release 1.0
 * @license CC-BY-NC
*/
include_once(_PS_MODULE_DIR_.'tallerordenes/models/Tallermarca.php');
include_once(_PS_MODULE_DIR_.'tallerordenes/models/Tallerfalla.php');
include_once(_PS_MODULE_DIR_.'tallerordenes/models/Tallermodel.php');

/* Carga del modelo, de datos, yo pongo los querys de creacion directo en el modelo */
class AdminTallerMainController extends ModuleAdminController	// el nombre del controlador llamada en la "tab" debe terminar en "Controller"
{
	private $employee;
	
	public function __construct()
	{
		$this->table = 'tallerorden';			// nombre de la tabla en db, ojo mas complejo con idiomas
		$this->className = 'Tallerorden';	// nombre de la clase que representa esa tabla
		$this->lang = false;
		//$this->deleted = false;
		$this->addRowAction('edit');
		$this->addRowAction('delete');
		$this->bulk_actions = 
			array('delete' => 
				array(
					'text' => $this->l('Borrar ordenes'), 
					'confirm' => $this->l('Realmente borrar estas ordenes?'),
					'icon' => 'icon-trash'
					)
				);
		$this->context = Context::getContext();
		// définition de l'upload, informe orden (no usado aun)
		$this->fieldImageSettings = array('name' => 'image', 'dir' => 'tallerordenes');
		$this->bootstrap = true;
		$this->id_employee = $this->context->cookie->id_employee;
		$producto_marcas = Tallermarca::getTallerMarcasbyId();
		foreach ($producto_marcas as $pm) {
			$this->producto_marcas_array[$pm['id_tallermarca']] = $pm['producto_marca'];
		}
		$producto_models = Tallermodel::getTallerModelsbyId();
		foreach ($producto_models as $pm) {
			$this->producto_models_array[$pm['id_tallermodel']] = $pm['producto_model'];
		}
		$producto_fallas = Tallerfalla::getTallerFallasbyId();
		foreach ($producto_fallas as $pm) {
			$this->producto_fallas_array[$pm['id_tallerfalla']] = $pm['producto_falla'];
		}
		$producto_employees = Employee::getEmployees(false); // false due get all employees not only active
		foreach ($producto_employees as $pm) {
			$this->producto_employ_array[$pm['id_employee']] = $pm['firstname'] . ' ' . $pm['lastname'];
		}
		$this->taller_proceso_array = array(
		  array('id_p' => 0,'namep' => 'Recibido'),
		  array('id_p' => 1,'namep' => 'No reparado'),
		  array('id_p' => 2,'namep' => 'Revisado'),
		  array('id_p' => 3,'namep' => 'Sin repuesto'),
		  array('id_p' => 4,'namep' => 'Reparado'),
		  array('id_p' => 5,'namep' => 'Entregado'),
		  array('id_p' => 6,'namep' => 'Incompleto'),
		);
		parent::__construct();
	}

	public function setFieldlist()
	{
		$this->fields_list = array(
			'id_tallerorden' => array(
				'title' => $this->l('ID'),
				'align' => 'center',
				'width' => 25
			),
			'date_recepcion' => array(
				'title' => $this->l('Recibido'),
				'width' => 'auto',
			),
			'id_employee_recv' => array(
				'title' => $this->l('Por'),
				'width' => 'auto',
			),
			'id_employee_tech' => array(
				'title' => $this->l('Tecnico'),
				'width' => 'auto',
			),
/*			'id_producto_falla' => array(
				'title' => $this->l('Fallas detectadas'),
				'type' => 'select', 
				'list' => $this->producto_fallas_array, 
				'filter_key' => 'id_tallerfalla',
				'filter_type' => 'int',
			),*/
			'cliente_nombre' => array(
				'title' => $this->l('Nombre cliente'),
				'width' => 'auto',
			),
			'id_producto_marca' => array(
				'title' => $this->l('Marca'), 
				'type' => 'select', 
				'list' => $this->producto_marcas_array, 
				'filter_key' => 'id_tallermarca',
				'filter_type' => 'int',
			),
			'id_producto_model' => array(
				'title' => $this->l('model'), 
				'type' => 'select', 
				'list' => $this->producto_models_array, 
				'filter_key' => 'id_tallermodel',
				'filter_type' => 'int',
			),
			'active' => array(
				'title' => $this->l('Estado'),
				'active' => 'status',
				'type' => 'bool',
				'orderby' => false,
				'class' => 'fixed-width-sm'
			),
			'date_modifica' => array(
				'title' => $this->l('Alterado'),
				'width' => 'auto',
			),
			'id_employee' => array(
				'title' => $this->l('Por'),
				'width' => 'auto',
			),
		);
	}

	/**
	 * Function used to render the list to display for this controller
	 */
	public function renderList()
	{
		$this->addRowAction('details');

		$this->setFieldlist();
		$lists = parent::renderList();

		parent::initToolbar();

		return $lists;
	}

	/* formulario del cliente hecho por el empleado, para generar una orden de reparacion*/
	public function renderForm()
	{
		if (!($obj = $this->loadObject(true)))
			return;

		$employe_id_modf = $this->context->employee->id_employee;
		$progresoactual = 1;
		$activestatus = true;
		if ( !empty($obj->id_tallerorden) AND $obj->id_tallerorden != '' )
		{
			$this->fields_value['id_tallerfalla[]'] = explode(',',$obj->id_tallerfalla);
			$employe_id_modf = $obj->id_employee;
			$progresoactual = $obj->id_tallerproceso;
			if ( $progresoactual == 5 )
				$activestatus = false;
		}
		$this->fields_value['progresoactual'] = $progresoactual;
		$this->fields_value['date_modifica'] = date("Y-m-d");
		$this->fields_value['id_employee'] = $employe_id_modf;


		$this->fields_form = array(
			'tinymce' => true,
			'legend' => array(
				'title' => $this->l('Ordenes reparaciones'),
				'icon' => 'icon-envelope-alt'
			),
			'input' => array(
				array(
					'type' => 'date',			'label' => $this->l('Fecha recepcion (auto):'),
					'required' => false,		'col' => 6,
					'name' => 'date_recepcion',
				),
				array(
					'type' => 'text',			'label' => $this->l('ID'),
					'name' => 'id_tallerorden',
					'required' => false,		'col' => 4,
					'hint' => $this->l('numero de orden, sera generada automaticamente'),
					'readonly' => true,
				),
				array(
					'type' => 'text',			'label' => $this->l('Nombre Cliente'),
					'name' => 'cliente_nombre',
					'required' => true,			'col' => 6,
					'hint' => $this->l('Nombre cliente.'),
				),
				array(
					'type' => 'textarea',			'label' => $this->l('Contacto Cliente'),
					'name' => 'cliente_contacto',
					'required' => true,			'col' => 6,
					'hint' => $this->l('Contacto cliente.'),
				),
				array(
					'type' => 'text',			'label' => $this->l('Serial producto:'),
					'name' => 'producto_serial',
					'required' => true,			'col' => 6,
				),
				array(
					'type' => 'select',
					'label' => $this->l('Fallas'),
					'name' => 'id_tallerfalla[]',
					'multiple' => true,
					'options' => array(
						'query' => Tallerfalla::getTallerFallasbyId(),
						'id' => 'id_tallerfalla',
						'name' => 'producto_falla'
					)
				),
				array(
					'type' => 'textarea',			'label' => $this->l('Trabajo realizar:'),
					'name' => 'producto_trabajo',
					'required' => false,			'col' => 6,
				),
				array(
					 'type' => 'select',
					'label' => $this->l('Asignado'),
					'name' => 'id_employee_tech',
					'required' => false,
					'col' => '6',
					'default_value' => (int)Tools::getValue('id_employee_tech'),
					'options' => array(
						'query' => Employee::getEmployees(true), // el true es que solo los que estan activos
						'id' => 'id_employee',
						'name' => 'firstname',
						'default' => array(
							'value' => '',
							'label' => $this->l('ninguno')
						)
					)
				),
				array(
					'type' => 'hidden',
					'name' => 'id_employee_recv',
					'required' => true,
				),
				array(
					'type' => 'hidden',
					'name' => 'id_employee',
					'required' => true,
				),
				array(
					 'type' => 'select',
					'label' => $this->l('Marca'),
					'name' => 'id_tallermarca',
					'required' => true,
					'col' => '6',
					'default_value' => (int)Tools::getValue('id_tallermarca'),
					'options' => array(
						'query' => Tallermarca::getTallerMarcasbyId(),
						'id' => 'id_tallermarca',
						'name' => 'producto_marca'
					)
				),
				array(
					 'type' => 'select',
					'label' => $this->l('model'),
					'name' => 'id_tallermodel',
					'required' => true,
					'col' => '6',
					'default_value' => (int)Tools::getValue('id_tallermodel'),
					'options' => array(
						'query' => Tallermodel::getTallerModelsbyId(),
						'id' => 'id_tallermodel',
						'name' => 'producto_model'
					)
				),
				array(
					'type' => 'radio',
					'label' => $this->l('Estado'),
					'name' => 'active',
					'required' => true,
					'is_bool' => true,
					'readonly' => true,
                    'values' => array(
						array(
							'id' => 'active_on',
							'value' => 1,
							'label' => $this->l('Taller')
						),
						array(
							'id' => 'active_off',
							'value' => 0,
							'label' => $this->l('Entregado')
						)
					),
					'hint' => $this->l('Indica si la orden ha sido recibida o fue ya de salida del taller.')
				),
				array(
					'type' => 'select',
					'label' => $this->l('Progreso'),
					'name' => 'id_tallerproceso',
					'required' => true,
					'col' => '6',
					'default_value' => (int)Tools::getValue('id_tallerproceso'), //(int)$progresoactual,
					'options' => array(
						'query' => $this->taller_proceso_array,
						'id' => 'id_p',
						'name' => 'namep'
					)
				),
				array(
					'type' => 'hidden',
					'name' => 'date_creacion',
				)
				
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'class' => 'button',
				'name' => 'submitTallerOrden'
			)
		);

		return parent::renderForm();
	}


	public function postProcess()
	{
		if (Tools::isSubmit('submitTallerOrden')) 
		{
			$version = str_replace('.', '', _PS_VERSION_);
			$version = substr($version, 0, 2);
 			$date_creacion = Tools::getValue('date_creacion',date("Y-m-d"));
			if ( $date_creacion == '0000-00-00' OR $date_creacion == 0)
				$_POST['date_creacion'] = date("Y-m-d");
			$id_employee_recv = Tools::getValue('id_employee_recv', $this->id_employee);
			if ( $id_employee_recv == '0' OR $id_employee_recv == 0)
				$_POST['id_employee_recv'] = (int)Tools::getValue('id_employee', $this->id_employee);
			$progresoactual = (int)Tools::getValue('id_tallerproceso');
			if ( $progresoactual == 5 )
			{
				$_POST['active'] = false;
				$_POST['date_salida'] = date("Y-m-d");
			}
			$_POST['id_tallerfalla'] = implode(',', Tools::getValue('id_tallerfalla'));
			$_POST['date_modifica'] = date("Y-m-d");
 		}
		parent::postProcess();
	}

	public function initPageHeaderToolbar()
	{
		$this->initToolbar();
		if (empty($this->display)) {
			$this->page_header_toolbar_btn['new_order'] = array(
				'href' => self::$currentIndex.'&addtallerorden&token='.$this->token,
				'desc' => $this->l('Crear nueva orden reparacion', null, null, false),
				'icon' => 'process-icon-new'
			);
			$this->page_header_toolbar_btn['new_marca'] = array(
				'href' => self::$currentIndex.'&addtallermarca&token='.$this->token,
				'desc' => $this->l('Add new marca', null, null, false),
				'icon' => 'process-icon-new'
			);
			 $this->page_header_toolbar_btn['new_model'] = array(
				'href' => self::$currentIndex.'&addtallermodel&token='.$this->token,
				'desc' => $this->l('Add new model', null, null, false),
				'icon' => 'process-icon-new'
			);
		}

		parent::initPageHeaderToolbar();
	}
}
