<?php
/*
* 2007-2016 VenenuX
*
*  @author PICCORO Lenz McKAY <mckaygerhard@gmail.com>
*  @copyright  2017 VenenuX
*  @license    http://opensource.org/licenses/osl-3.0.php  Open Software License (OSL 3.0)
*/

// inclusion de modelo de datos y definicion de tabla sql
include_once(_PS_MODULE_DIR_.'tallerordenes/models/Tallerfalla.php');

/* Carga del modelo, de datos, yo pongo los querys de creacion directo en el modelo
 * @property Falla $object
 */
class AdminTallerfallaController extends ModuleAdminController
{
	public function __construct()
	{
		$this->bootstrap = true;
		$this->table = 'tallerfalla';
		$this->className = 'Tallerfalla';
		$this->lang = false;
		$this->addRowAction('edit');
		$this->addRowAction('delete');
		$this->bulk_actions = array(
			'delete' => array(
				'text' => $this->l('Delete selected'),
				'confirm' => $this->l('Delete selected items?'),
				'icon' => 'icon-trash'
			)
		);
		$this->context = Context::getContext();
		$this->id_employee = $this->context->cookie->id_employee;

		$this->fields_list = array(
			'id_tallerfalla' => array('title' => $this->l('ID'), 'align' => 'center', 'class' => 'fixed-width-xs'),
			'producto_falla' => array('title' => $this->l('Nombre falla')),
			'date_creacion' => array('title' => $this->l('Creado')),
			'date_modifica' => array('title' => $this->l('Modificado')),
			'id_employee' => array('title' => $this->l('Por')),
		);

		parent::__construct();
	}

	public function renderForm()
	{


		$this->fields_form = array(
			'legend' => array(
				'title' => $this->l('Fallas en taller'),
				'icon' => 'icon-envelope-alt'
			),
			'input' => array(
				array(
					'type' => 'text',
					'label' => $this->l('Falla'),
					'name' => 'producto_falla',
					'required' => true,
					'lang' => false,
					'col' => 6,
				),
				array(
					'type' => 'hidden',
					'name' => 'date_modifica',
					'lang' => false,
				),
				array(
					'type' => 'hidden',
					'name' => 'date_creacion',
					'lang' => false,
				),
				array(
					'type' => 'hidden',
					'name' => 'id_employee',
					'lang' => false,
				),
			),
			'submit' => array(
				'title' => $this->l('Save'),
				'name' => 'submit'.$this->className,
			)
		);

        $this->fields_value['date_modifica'] = date("Y-m-d");
		$this->fields_value['id_employee'] = (int)Tools::getValue('id_employee', $this->id_employee);

		return parent::renderForm();
	}

    public function postProcess()
	{
		if (Tools::isSubmit('submit'.$this->className)) 
        {
			$date_creacion = Tools::getValue('date_creacion',date("Y-m-d"));
			if ( $date_creacion == '0000-00-00' OR $date_creacion == 0)
				$_POST['date_creacion'] = date("Y-m-d");
	    }
        parent::postProcess();
	}

	public function initPageHeaderToolbar()
	{
		$this->initToolbar();
		if (empty($this->display)) {
			$this->page_header_toolbar_btn['new_contact'] = array(
				'href' => self::$currentIndex.'&addtallerfalla&token='.$this->token,
				'desc' => $this->l('Add new falla', null, null, false),
				'icon' => 'process-icon-new'
			);
		}

		parent::initPageHeaderToolbar();
	}

	/** reimplement borrar una falla pero si esta no esta usandose (*/
	public function delete()
	{
		$howmanyc = 0;
		$result = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'tallerorden` WHERE `id_producto_falla` = '.(int)$this->id.' LIMIT 1');
		foreach ($result as $res)
			$howmanyc = $howmanuc + 1;
		if ($howmanyc > 0)
			return FALSE;
		return (parent::delete());
	}
}
