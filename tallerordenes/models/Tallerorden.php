<?php
/*
* 2017 PICCORO Lenz McKAY
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    Lenz McKAY Gerhard <mckaygerhard@gmail.com>
*  @copyright 2017 Lenz McKAY Gerhard <mckaygerhard@gmail.com>
*  @version   1.1.1
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class Tallerorden extends ObjectModel
{

	public $id_tallerorden;

	public $cliente_nombre; 		// este es el cliente, debera ser parte de prestashop
	public $cliente_contacto; 			// por si multitienda, multiidioma no todavia
	public $id_tallermodel;
	public $producto_serial;
	public $id_tallermarca;
	public $id_tallerfalla;
	public $producto_trabajo;

	public $active = true;			// si true=recibida si false=salida
	public $id_tallerproceso;	// 1 recibido 2 revisado, 3 faltan repuestos, 4 reparado, 0 no reparado

	public $id_informe;			// informe final al terminar reparar

	public $date_creacion;		// cuando ingresa o se crea la orden de taller
	public $date_modifica;		// cuando se altero este registro por ultima vez
	public $date_recepcion;		// el ingreso a mano, no debe diferir mucho
	public $date_salida;		// cuando sale o se lo lleva el cliente del taller
	public $employe_id;			// ultimo empleado que modifica la orden
	public $id_employee_tech;	// tecnico que le atendera
	public $id_employee_recv;	// empleado que recibio la orden

	/**
	 * definicion cuando se devuelven la estructura de un dato, 
	 * el array php tendra esa estructura ahora para cualqueir dato desde la db
	 */
	public static $definition = array(
		'table' => 'tallerorden',
		'primary' => 'id_tallerorden',
		'fields' => array(
			'id_producto' =>		array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false,'size' => 255),
			'id_employee' =>		array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false,'size' => 255),
			'id_employee_tech' =>		array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false,'size' => 255),
			'id_employee_recv' =>		array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true,'size' => 255),
			'cliente_nombre' =>		array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true,'size' => 255),
			'cliente_contacto' =>	array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true,'size' => 255),
			'id_tallermodel' =>		array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true,'size' => 255),
			'producto_serial' =>	array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true,'size' => 255),
			'id_tallermarca' =>		array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true,'size' => 255),
			'id_tallerfalla' =>		array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false,'size' => 255),
			'producto_trabajo' =>		array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => false,'size' => 255),
			'active' =>				array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'id_tallerproceso' => 	array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true,'size' => 255),
			'date_creacion' =>		array('type' => self::TYPE_DATE, 'validate' => 'isDate'), // cuando ingresa o se crea la orden de taller
			'date_modifica' =>	array('type' => self::TYPE_DATE), // cuando se altera
			'date_recepcion' =>		array('type' => self::TYPE_DATE, 'validate' => 'isDate'), // cuando ingresa o se crea la orden de taller
			'date_salida' =>		array('type' => self::TYPE_DATE), // cuando ingresa o se crea la orden de taller
		),
	);

	/** crea la estructura de datos para las ordenes de reparacion de taller */
	public static function createTallerOrdenTable()
	{
		$sql = 'CREATE TABLE `'._DB_PREFIX_.'tallerorden`(
			`id_tallerorden` int(10) unsigned NOT NULL auto_increment COMMENT \'id de orden de taller\',
			`id_employee` int(10) unsigned NULL ,
			`id_employee_tech` int(10) unsigned NULL ,
			`id_employee_recv` int(10) unsigned NULL ,
			`id_producto` int(10) unsigned NULL COMMENT \'id de tienda en que aplica el orden de taller (prestashop)\',
			`id_lang` int(10) unsigned NULL COMMENT \'id de idioma, deberia ser el mismo de la tienda (prestashop)\',
			`cliente_nombre` text,
			`cliente_contacto` text,
			`id_tallermodel` text,
			`producto_serial` text,
			`id_tallermarca` text,
			`id_tallerfalla` text NULL,
			`producto_trabajo` text NULL COMMENT \'descripcion de la falla\',
			`active` tinyint(1) unsigned NOT NULL DEFAULT \'0\',
			`id_tallerproceso` text NULL ,
			`date_creacion` text NULL ,
			`date_modifica` text NULL ,
			`date_recepcion` text NULL ,
			`date_salida` text NULL ,
			PRIMARY KEY (`id_tallerorden`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';
		$returnvalue = Db::getInstance()->execute($sql);
		if ( $returnvalue === FALSE ) return FALSE;
		return TRUE;
	}

	/** eliminacion de la estructura de tablas del modulo */
	public static function dropTallerOrdenTable()
	{
		$sql = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'tallerorden`	';
		$result = Db::getInstance()->execute($sql);
		return $result;
	}

	/** obtiene una lista de ordenes de reparaciones para mostrarlas en el sistema */
	public static function getOrdenesTaller($id_customer = null, $id_lang = null, $id_shop = 1)  // basado en getFavoriteProducts
	{
		$shop = Context::getContext()->shop;
		$filtrado = '';
		if ( $id_customer != null and trim($id_customer) != '')
			$filtrado = 'AND p.id_customer = '.(int)$id_customer;
		if ( $id_shop == null )
			$id_shop = $shop->id;
		return Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT DISTINCT p.*
			FROM `'._DB_PREFIX_.'tallerorden` p
			WHERE 1 = 1 '.$filtrado.' ORDER BY p.date_creacion'
		);
	}

	/** reimplement borrar una orden pero si esta no esta cerrada (*/
	public function delete()
	{
		$howmanyc = 0;
		$result = Db::getInstance()->executeS('SELECT * FROM `'._DB_PREFIX_.'tallerorden` WHERE `id_tallerorden` = '.(int)$this->id_tallerorden.' AND `active` != 0 LIMIT 1');
		foreach ($result as $res)
			$howmanyc = $howmanuc + 1;
		if ($howmanyc > 0)
			return FALSE;
		return (parent::delete());
	}

}
