<?php
/*
* 2017 PICCORO Lenz McKAY
*
*  @author    Lenz McKAY Gerhard <mckaygerhard@gmail.com>
*  @copyright 2017 Lenz McKAY Gerhard <mckaygerhard@gmail.com>
*  @version   1.1.1
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class Tallerfalla extends ObjectModel
{

	public $id_tallerfalla;

	public $producto_falla;
	public $date_creacion;		// cuando ingresa o se crea la orden de taller
	public $date_modifica;		// cuando se altero este registro por ultima vez
	public $id_employee;			// ultimo empleado que modifica la orden

	/**
	 * definicion cuando se devuelven la estructura de un dato,
	 * el array php tendra esa estructura ahora para cualqueir dato desde la db
	 */
	public static $definition = array(
		'table' => 'tallerfalla',
		'primary' => 'id_tallerfalla',
		'fields' => array(
			'producto_falla' =>	array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true,'size' => 255),
			'date_creacion' =>	array('type' => self::TYPE_DATE, 'validate' => 'isDate'), // cuando ingresa o se crea
			'date_modifica' =>	array('type' => self::TYPE_DATE, 'validate' => 'isDate'), // cuando se altera
			'id_employee' =>	array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true,'size' => 255),
		),
	);

	/** crea la estructura de datos para las ordenes de reparacion de taller */
	public static function createTallerFallaTable()
	{
		$sql = 'CREATE TABLE `'._DB_PREFIX_.'tallerfalla`(
			`id_tallerfalla` int(10) unsigned NOT NULL auto_increment ,
			`producto_falla` text,
			`date_creacion` text NULL ,
			`date_modifica` text NULL ,
			`id_employee` int(10) unsigned NULL ,
			PRIMARY KEY (`id_tallerfalla`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';
		$returnvalue = Db::getInstance()->execute($sql);
		return $returnvalue;
	}

	/** eliminacion de la estructura de tablas del modulo */
	public static function dropTallerFallaTable()
	{
		$sql = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'tallerfalla`	';
		$result = Db::getInstance()->execute($sql);
		return $result;
	}

	/** obtiene una lista de fallas de reparaciones para mostrarlas en el sistema */
	public static function getTallerFallasbyId($id_tallerfalla = null, $id_shop = 1)  // basado en getFavoriteProducts
	{
		$shop = Context::getContext()->shop;
		$filtrado = '';
		if ( $id_tallerfalla != null and trim($id_tallerfalla) != '')
			$filtrado = 'AND m.id_tallerfalla = '.(int)$id_tallerfalla;
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT m.*
			FROM `'._DB_PREFIX_.'tallerfalla` m
			WHERE 1 = 1 '.$filtrado.' ORDER BY m.producto_falla'
		);
		return $result;
	}

}
