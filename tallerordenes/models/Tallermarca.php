<?php
/*
* 2017 PICCORO Lenz McKAY
*
*  @author    Lenz McKAY Gerhard <mckaygerhard@gmail.com>
*  @copyright 2017 Lenz McKAY Gerhard <mckaygerhard@gmail.com>
*  @version   1.1.1
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*/

class Tallermarca extends ObjectModel
{

	public $id_tallermarca;

	public $producto_marca;
	public $date_creacion;		// cuando ingresa o se crea la orden de taller
	public $date_modifica;		// cuando se altero este registro por ultima vez
	public $id_employee;			// ultimo empleado que modifica la orden

	/**
	 * definicion cuando se devuelven la estructura de un dato,
	 * el array php tendra esa estructura ahora para cualqueir dato desde la db
	 */
	public static $definition = array(
		'table' => 'tallermarca',
		'primary' => 'id_tallermarca',
		'fields' => array(
			'producto_marca' =>	array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true,'size' => 255),
			'date_creacion' =>	array('type' => self::TYPE_DATE, 'validate' => 'isDate'), // cuando ingresa o se crea
			'date_modifica' =>	array('type' => self::TYPE_DATE, 'validate' => 'isDate'), // cuando se altera
			'id_employee' =>	array('type' => self::TYPE_STRING, 'validate' => 'isGenericName', 'required' => true,'size' => 255),
		),
	);

	/** crea la estructura de datos para las ordenes de reparacion de taller */
	public static function createTallerMarcaTable()
	{
		$sql = 'CREATE TABLE `'._DB_PREFIX_.'tallermarca`(
			`id_tallermarca` int(10) unsigned NOT NULL auto_increment ,
			`producto_marca` text,
			`date_creacion` text NULL ,
			`date_modifica` text NULL ,
			`id_employee` int(10) unsigned NULL ,
			PRIMARY KEY (`id_tallermarca`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';
		$returnvalue = Db::getInstance()->execute($sql);
		return $returnvalue;
	}

	/** eliminacion de la estructura de tablas del modulo */
	public static function dropTallerMarcaTable()
	{
		$sql = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'tallermarca`	';
		$result = Db::getInstance()->execute($sql);
		return $result;
	}

	/** obtiene una lista de marcas de reparaciones para mostrarlas en el sistema */
	public static function getTallerMarcasbyId($id_tallermarca = null, $id_shop = 1)  // basado en getFavoriteProducts
	{
		$shop = Context::getContext()->shop;
		$filtrado = '';
		if ( $id_tallermarca != null and trim($id_tallermarca) != '')
			$filtrado = 'AND m.id_tallermarca = '.(int)$id_tallermarca;
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT m.*
			FROM `'._DB_PREFIX_.'tallermarca` m
			WHERE 1 = 1 '.$filtrado.' ORDER BY m.producto_marca'
		);
		return $result;
	}

}
