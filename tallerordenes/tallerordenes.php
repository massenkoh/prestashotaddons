<?php
/**
* @copyleft 2017 venenux
*
* NOTE
* You are free edit and play around with the module.
* Please visit contentbox.org for more information.
*
*  @author    Lenz McKAY Gerhard <mckaygerhard@gmail.com>
*  @copyright 2017 Lenz McKAY Gerhard <mckaygerhard@gmail.com>
*  @version   1.1.2
* @license CC-BY-NC
*/

/* Security */
if (!defined('_PS_VERSION_'))
	exit;

/* Carga del modelo, de datos, yo pongo los querys de creacion directo en el modelo */ // se hace include porque la clase este siempre disponible para usarla
include_once(_PS_MODULE_DIR_.'tallerordenes/models/Tallerorden.php');
include_once(_PS_MODULE_DIR_.'tallerordenes/models/Tallermarca.php');
include_once(_PS_MODULE_DIR_.'tallerordenes/models/Tallerfalla.php');
include_once(_PS_MODULE_DIR_.'tallerordenes/models/Tallermodel.php');

/** clase principal del modulo prestasho */
class tallerordenes extends Module	// el guindosero prestashop busca por este nombre el archivo php, ES CASESENSITIVE en servidores reales
{
	private $_html = '';
	
	private $errors = null;

	public function __construct()
	{
		$this->name = 'tallerordenes';	// el guindosero prestashop busca por este nombre el archivo php, ES CASESENSITIVE en servidores reales
		$this->tab = 'front_office_features';
		$this->version = '1.1.1.4';
		$this->author = 'Sergio Párraga <computersystemsunic@gmail.com>, PICCORO Lenz McKAY <mckaygerhard@gmail.com>';
		//	Min version of PrestaShop wich the module can be install
		$this->ps_versions_compliancy['min'] = '1.6';
		// Max version of PrestaShop wich the module can be install
		$this->ps_versions_compliancy['max'] = _PS_VERSION_;
		// OR $this->ps_versions_compliancy = array('min' => '1.5', 'max' => '1.6');
		$this->_html = '';
		//	The need_instance flag indicates whether to load the module's class when displaying the "Modules" page
		//	in the back-office. If set at 0, the module will not be loaded, and therefore will spend less resources
		//	to generate the page module. If your modules needs to display a warning message in the "Modules" page,
		//	then you must set this attribute to 1.
		$this->need_instance = 0;
		$this->dependencies = array();	// Modules needed for install

		parent::__construct();

		$this->bootstrap = true;
		
		$this->displayName = $this->l('Tallerordenes');
		$this->description = $this->l('taller por ordenes de servicio');

		$this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
		$this->errors = array();
	}

	public function install()
	{
		// si se ve en todas las tiendas o solo la que esta activa
		if (Shop::isFeatureActive())
			Shop::setContext(Shop::CONTEXT_ALL);
		// menus en backend Tabs, este es el principal
		$parent_tab = new Tab();
		$parent_tab->class_name = 'AdminTallerMain'; // nombre del controlador, elnombre de la clase debe ser exacto igual case-sensitive en el controlador
		$parent_tab->name[$this->context->language->id] = $this->l('Taller'); // sin traducir esto, el submenu abajo traduce si existe su traduccion iterando en idiomas
		$parent_tab->id_parent = 0; // en parte de menu raiz principal, no submenu, sino Tab::getIdFromClassName('AdminTools'); si quiero en raiz de administracion como submenu
		$parent_tab->module = $this->name; // cuidado no es nombre de clase sino nombre de archivo realmente, que basura es prestashop
		$parent_tab->add();
		$tab = new Tab();
		$tab->class_name = 'AdminTallerMain';
		$tab->id_parent = $parent_tab->id; // es submenu del de arriba
		$tab->module = $this->name;
		$languages = Language::getLanguages(false);
        foreach($languages as $lang){   $tab->name[$lang['id_lang']] = 'Order';   }
		$tab->add();
		$tab = new Tab();
		$tab->class_name = 'AdminTallermarca';
		$tab->id_parent = $parent_tab->id; // es submenu del de arriba
		$tab->module = $this->name;
		$languages = Language::getLanguages(false);
		foreach($languages as $lang){   $tab->name[$lang['id_lang']] = 'Marcas';   }
        $tab->add();
		$tab = new Tab();
		$tab->class_name = 'AdminTallerfalla';
		$tab->id_parent = $parent_tab->id; // es submenu del de arriba
		$tab->module = $this->name;
		$languages = Language::getLanguages(false);
		foreach($languages as $lang){   $tab->name[$lang['id_lang']] = 'Fallas';   }
        $tab->add();
		$tab = new Tab();
		$tab->class_name = 'AdminTallermodel';
		$tab->id_parent = $parent_tab->id; // es submenu del de arriba
		$tab->module = $this->name;
		$languages = Language::getLanguages(false);
        foreach($languages as $lang){   $tab->name[$lang['id_lang']] = 'Modelos';   }
        $tab->add();
		//Inicializar config del modulo en la tabla principal de prestashop
		Configuration::updateValue('TALLERORDENES_CONFVAL', 'instalado');
		Configuration::updateValue('TALLERORDENES_CONFDEL', 'instalado');
		// registrar el gancho que dispara lso eventos segun la clase principal
		if ( !parent::install() || 
			!$this->registerHook('tallermanage') || 
			!Tallerfalla::createTallerFallaTable() ||
			!Tallerorden::createTallerOrdenTable() ||
			!Tallermarca::createTallerMarcaTable() ||
			!Tallermodel::createTallerModelTable() )
			return false;

		return true;
	}

	public function uninstall()
	{
		// borrar configuraciones
		Configuration::deleteByName('TALLERORDENES_CONFVAL');
		Configuration::deleteByName('TALLERORDENES_CONFDEL');
		// tabs son los menus del lado de backoffice, aqui quita los del modulo, por su nombre
		$moduleTabs = Tab::getCollectionFromModule($this->name);
		if (!empty($moduleTabs)) {
			foreach ($moduleTabs as $moduleTab) {
				$moduleTab->delete();
			}
		}

		if (!parent::uninstall() ||
			!$this->unregisterHook('tallermanage') || 
			!Tallerorden::dropTallerOrdenTable() || 
			!Tallermarca::dropTallerMarcaTable() ||
			!Tallerfalla::dropTallerFallaTable() ||
			!Tallermodel::dropTallerModelTable()
			) return false;
		
		return true;
	}


	// este le dice a prestachop que ofrece configurarse "theres a configuration page also"
	public function getContent()
	{
		$output = '<h2>'.$this->displayName.'</h2>';
		if (Tools::isSubmit('submit'.Tools::ucfirst($this->name)))
		{
			$ordenesval_conf = Tools::getValue('TALLERORDENES_CONFVAL');
			$ordenesdel_conf = Tools::getValue('TALLERORDENES_CONFDEL');
			// obtengo valores de configuracino de modulo
			Configuration::updateValue('TALLERORDENES_CONFVAL', $ordenesval_conf);
			Configuration::updateValue('TALLERORDENES_CONFDEL', $ordenesdel_conf);
			// si ocurre algun error:
			if (isset($this->errors) && count($this->errors))
				$output .= $this->displayError(implode('<br />', $this->errors));
			else
				$output .= $this->displayConfirmation($this->l('Taller configurado e iniciado'));
		}
		return $output.$this->displayForm(); // el form se contruye enteramente con el framework, sin tpl inclusive
	}

	public function displayForm()
	{
		$this->context->smarty->assign('request_uri', Tools::safeOutput($_SERVER['REQUEST_URI']));
		$this->context->smarty->assign('path', $this->_path);
		$this->context->smarty->assign('TALLERORDENES_CONFVAL', pSQL(Tools::getValue('TALLERORDENES_CONFVAL', Configuration::get('TALLERORDENES_CONFVAL'))));
		$this->context->smarty->assign('TALLERORDENES_CONFDEL', pSQL(Tools::getValue('TALLERORDENES_CONFDEL', Configuration::get('TALLERORDENES_CONFDEL'))));
		$this->context->smarty->assign('submitName', 'submit'.Tools::ucfirst($this->name));
		$this->context->smarty->assign('errors', $this->errors);
		// con el framework se puede hacer todo aqui, con smarty (1.5/1.6) plantillas, con symphony (1.7) no se que
		return $this->display(__FILE__, 'views/templates/admin/configure.tpl');
	}

	public function hookTallermanage($params)
	{
		/* aqui posible codigo para ver las ordenes listadas y los datos sensibles REQUIERE controller */
		$params = $params;
		return true;
	}
}


