<?php
/*
 * Tab Taller - Controller Admin para taller de servicios de reparaciones (producto entra y sale , no venta)
 *
 * @author     	Sergio Párraga <computersystemsunic@gmail.com>, PICCORO Lenz McKAY <mckaygerhard@gmail.com>
 * @copyright  	2017 PICCORO Lenz McKAY <mckaygerhard@gmail.com>
 * @version   	1.0
 * @link       	http://datasystems.com.ve
 * @since      	File available since Release 1.0
 * @license CC-BY-NC
*/
				    	
header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header("Last-Modified: ".gmdate("D, d M Y H:i:s")." GMT");
						
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
						
header("Location: ../");
exit;
