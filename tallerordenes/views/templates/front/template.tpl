{*
 * @author     	Sergio Párraga <computersystemsunic@gmail.com>, PICCORO Lenz McKAY <mckaygerhard@gmail.com>
 * @copyright  	2017 PICCORO Lenz McKAY <mckaygerhard@gmail.com>
 * @version   	1.0
 * @link       	http://datasystems.com.ve
 * @since      	File available since Release 1.0
*}
{$content|escape:nofilter}
